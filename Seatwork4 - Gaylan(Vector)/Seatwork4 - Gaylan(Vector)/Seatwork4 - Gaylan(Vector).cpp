// Seatwork # 4 - Vector
#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "UnorderedArray.h"

using namespace std;

void main()
{
	int removingElement;
	int inputSize;
	int inputNumber;
	int index;

	cout << "How big is the size of your array?";
	cin >> inputSize;

	UnorderedArray<int> integers(inputSize);

	for (int i = 0; i <= inputSize; i++)
		integers.push(rand() % 100 + 1);

	for (int i = 0; i < integers.getSize(); i++)
		cout << integers[i] << " ";

	cout << endl;

	cout << "Remove an Integer. (index number)" << endl;
	cin >> removingElement;

	integers.remove(removingElement);

	cout << endl;

	for (int i = 0; i < integers.getSize(); i++)
		cout << integers[i] << " ";
	
	cout << "Put a number between 1 to 100." << endl;
	cin >> inputNumber;

	index = integers.linearSearch(inputSize, inputNumber);
	if (index == -1)
		cout << "The Value is not present in the array." << endl;
	else
		cout << "The value is found in position " << index << endl;

	system("pause");
}


//// Linear search Function
////int search(int arr[], int n, int x) 
//{
//	int i;
//	for (i = 0; i < n; i++)
//		if (arr[i] == x)
//			return i;
//	return -1;
//}
//
//// Driver code 
//int main()
//{
//	int arr[] = { 3, 4, 1, 7, 5 };
//	int n = sizeof(arr) / sizeof(arr[0]);
//	int x = 4;
//
//	int index = search(arr, n, x);
//	if (index == -1)
//		cout << "Element is not present in the array";
//	else
//		cout << "Element found at position " << index;
//
//	return 0;
//}