#pragma once
#include <assert.h>

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;

			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);

			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		int i = mNumElements - 1;
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
		{
			expand();
		}

		while (value < mArray[i] && i >= 0)
		{
			mArray[i + 1] = mArray[i];
			i--;
		}
		mArray[i + 1] = value;
		mNumElements++;
	}

	virtual T& operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	int getSize()
	{
		return mNumElements;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual int binarySearch(int searchSize ,int searchElement)
	{
		assert(mArray != NULL);

		mMaxSize = searchSize;

		int r = mNumElements - 1;
		int first = 0;

		while (first <= r)
		{
			int middle = first + (r - first) / 2;

			if (mArray[middle] == searchElement)
				return middle;

			if (mArray[middle] > searchElement)
				first = middle + 1;

			else
				r = middle - 1;
		}

		return -1;
	}

private:
	T* mArray;
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T* temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	}

};
