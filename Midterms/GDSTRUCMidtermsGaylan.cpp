// GDSTRUCMidtermsGaylan.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
#include <vector>
#include "UnorderedArray.h"
#include "OrderedArray.h"

using namespace std;

void main()
{
	int inputSize;
	int inputNumber;
	int result;

	cout << "Enter size of array: ";
	cin >> inputSize;

	UnorderedArray<int> unorder(inputSize);
	OrderedArray<int> order(inputSize);

	for (int i = 0; i < inputSize; i++)
	{
		int random = rand() % 100 + 1;
		unorder.push(random);
		order.push(random);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unorder.getSize(); i++)
		cout << unorder[i] << " ";

	cout << endl;
	cout << "\nOrdered: ";
	for (int i = 0; i < order.getSize(); i++)
		cout << order[i] << " ";

	cout << endl;

	cout << "\nEnter number to search: ";
	cin >> inputNumber;
	cout << endl;

	cout << "Unordered Array(Linear Search):\n";
	result = unorder.linearSearch(inputSize, inputNumber); // linear Search
	if (result == -1)
		cout << "The Value is not present in the array." << endl;
	else
		cout << "The value is found in position " << result << endl;

	cout << "Ordered Array(Binary Search):\n";
	result = order.binarySearch(inputSize, inputNumber);
	if(result >= 0)
		cout << inputNumber << " was found at index " << result << ".\n";
	else
		cout << inputNumber << " not found." << endl;

	system("pause");
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
