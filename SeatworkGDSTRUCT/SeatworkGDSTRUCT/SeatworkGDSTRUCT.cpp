#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

int main()
{
	int array[10];
	int holder;
	string choice;
	int numChoice;

	for (int i = 0; i < 10; i++)
	{
		int randomization = rand() % 69 + 1;

		array[i] = randomization;
		cout << "Random Array: " << array[i] << endl;
	}

	cout << "Do you want it in ascending or descending? (ascending/descending)" << endl;
	cin >> choice; // choosing if ascending or descending.

	if (choice == "ascending")
	{
		for (int primary = 0; primary < 10; ++primary)
		{
			for (int secondary = 0; secondary < (10 - primary); ++secondary)
			{
				if (array[secondary] > array[secondary + 1])
				{
					holder = array[secondary];
					array[secondary] = array[secondary + 1];
					array[secondary + 1] = holder;
				}
			}
		}
	}
	else if (choice == "descending")
	{
		for (int primary = 0; primary < 10; ++primary)
		{
			for (int secondary = 0; secondary < (10 - primary); ++secondary)
			{
				if (array[secondary] < array[secondary + 1])
				{
					holder = array[secondary];
					array[secondary] = array[secondary + 1];
					array[secondary + 1] = holder;
				}
			}
		}
	}

	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << endl;
	}

	// linear search

	cout << "Among the numbers, which one do you choose? ";
	cin >> numChoice;

	for (int i = 0; i < 10; i++)
	{
		if (array[i] == numChoice)
		{
			cout << "The element is present within the array at index." << endl;
			break;
		}
		else
		{
			cout << "The element is not present in the array." << endl;
		}
	}

	system("pause");
return 0;
}