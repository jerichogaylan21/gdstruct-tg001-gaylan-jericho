// GDSTRUCT - FinalExam.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <vector>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	int inputSize;
	int pushInput;
	int inputNumber;
	int choice;
	int firstQueue;

	cout << "Enter size of element sets: ";
	cin >> inputSize;

	UnorderedArray<int> Stack(inputSize);
	UnorderedArray<int> Queue(inputSize);

	system("pause");
	system("cls");

	while (true)
	{
		cout << "What do you want to do?" << endl;
		cout << "1 - Push Elements" << endl;
		cout << "2 - Pop Elements" << endl;
		cout << "3 - Print everything then empty set" << endl;

		cin >> choice;

		if (choice == 1)
		{
			cout << "Enter Number: ";
			cin >> pushInput;
			Stack.push(pushInput);
			Queue.push(pushInput);

			cout << endl << endl;

			cout << "Top Element of Sets:" << endl;
			
			for (int i = 0; i < 1; i++)
			{
				cout << "Queue: " << Queue[0] << endl;
			}
			cout << "Stack: " << pushInput << endl;
			system("pause");
			system("cls");
		}
		else if (choice == 2)
		{
			Stack.popBack();
			Queue.pop(0);

			cout << "You popped the front elements." << endl;

			for (int i = 0; i < 1; i++)
			{
				cout << "Stack: " << Stack[0] << endl;
				cout << "Queue: " << Queue[0] << endl;
			}
			system("pause");
			system("cls");
		}
		else if (choice == 3)
		{
			for (int i = 0; i < Stack.getSize(); i++)
			{
				cout << Stack[i] << " ";
			}

			cout << endl;

			for (int i = 0; i < Stack.getSize(); i++)
			{
				cout << Queue[i] << " ";
			}

			cout << endl;


			Stack.deleteAll();
			Queue.deleteAll();
			break;
		}
	}


	system("pause");
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
